#include "parametrypomocnicze.h"

ParametryPomocnicze::ParametryPomocnicze(Parametry p)
{
    this->param = p;
    b = policzB();
    I =policzI() ;
    omega_0 = policzOmega_0();
    policzDelteIwsp();
    omega_1= policzOmega_1();
    epsilon_1 = policzEpsilon_1();
    omega_2 = policzOmega_2();
    epsilon_2 = policzEpsilon_2();
    beta_1 = policzBeta_1();
    beta_2 = policzBeta_2();
    a_11 = policz_a_11();
    a_12 = policz_a_12();
    a_21 = policz_a_21();
    a_22 = policz_a_22();
    B_1 = policz_B_1();
    B_2 = policz_B_2();
}

double ParametryPomocnicze::policzB()
{
    return ((1.0/2.0)*param.L);
}

double ParametryPomocnicze::policzI()
{
    return (1.0/12.0) * (param.m * param.L*param.L);
}

double ParametryPomocnicze::policzBeta_1()
{
    return ((1.0+(param.M/param.m))*omega_1*omega_1-omega_0*omega_0)/(b*omega_1*omega_1);
}

double ParametryPomocnicze::policzBeta_2()
{
    return ((1.0+(param.M/param.m))*omega_2*omega_2-omega_0*omega_0)/(b*omega_2*omega_2);

}

void ParametryPomocnicze::policzDelteIwsp()
{
    wspolczynnik_b = ((-1.0) * (1.0 + (param.M/param.m)) * param.g * b - (b * b + (I)/(param.m)) * omega_0 * omega_0);
    wspolczynnik_a = ((1.0 + (param.M/param.m)) * (b*b +(I/param.m)) - b*b);
    wspolczynnik_c = omega_0 * omega_0 * param.g * b;
    delta = sqrt((wspolczynnik_b * wspolczynnik_b) - 4.0 * (wspolczynnik_a * wspolczynnik_c));
}

double ParametryPomocnicze::policzOmega_1()
{

    return sqrt((-wspolczynnik_b-delta)/(2.0 * wspolczynnik_a));
}

double ParametryPomocnicze::policzEpsilon_1()
{
    return atan(((param.fi_0-param.x_0*beta_2)/(param.omega_p-param.v_0*beta_2))*omega_1);
}

double ParametryPomocnicze::policzOmega_2()
{
     return sqrt((-wspolczynnik_b+delta)/(2.0 *wspolczynnik_a));
}

double ParametryPomocnicze::policzEpsilon_2()
{
    return atan(((param.fi_0-param.x_0*beta_1)/(param.omega_p-param.v_0*beta_1))*omega_2);
}
//drgania masy dla omegi1
double ParametryPomocnicze::policz_a_11()
{
    return (param.x_0 - a_12 * sin(epsilon_2)) / sin(epsilon_1);
}

double ParametryPomocnicze::policz_a_12()
{
    return (param.fi_0-beta_1*param.x_0)/((beta_2-beta_1)*sin(epsilon_2));
}

double ParametryPomocnicze::policz_a_21()
{
    return a_11*beta_1;
}

double ParametryPomocnicze::policz_a_22()
{
    return a_12*beta_2;
}


double ParametryPomocnicze::policz_B_1()
{
    double A = (param.k * param.Xk)* (param.m * (param.g - param.OMEGA * param.OMEGA - I * param.OMEGA * param.OMEGA));
    double B = (param.m * b * param.OMEGA * param.OMEGA);
    double C = param.k -((param.m + param.M)*param.OMEGA*param.OMEGA);
    double D = param.m * b * (param.g - param.OMEGA * param.OMEGA) - I * param.OMEGA*param.OMEGA;
    return A / (B - C * D);
}

double ParametryPomocnicze::policz_B_2()
{
    return -(2.0 * param.k * param.Xk)/(param.m * b * param.OMEGA * param.OMEGA);
}

double ParametryPomocnicze::policzOmega_0()
{
    return omega_0 = sqrt(param.k / param.m);
}

