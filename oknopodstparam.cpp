#include "oknopodstparam.h"
#include "ui_oknopodstparam.h"

OknoPodstParam::OknoPodstParam(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OknoPodstParam)
{
    ui->setupUi(this);
    pobierzParametry();


}

OknoPodstParam::~OknoPodstParam()
{
    delete ui;
}


void OknoPodstParam::pobierzParametry()
{
    M = ui->M->value();
    m = ui->m->value();
    L = ui->L->value();
    k = ui->k->value();
    g = ui->g->value();
    x_0 = ui->x_0->value();
    fi_0 = ui->fi_0->value();
    v_0 = ui->v_0->value();
    omega_p = ui->omega_p->value();
//    omega_0 = ui->omega_0->value();
}





void OknoPodstParam::on_buttonBox_accepted()
{
    pobierzParametry();
}
