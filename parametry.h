#ifndef PARAMETRY_H
#define PARAMETRY_H
#include "math.h"


class Parametry
{
public:
    Parametry();

    //masa bloczka
    double M;

    //masa wahadła
    double m;
    //długosc wahadła
    double L;

    //sprężystość sprężyny
    double k;

    //amplituda wymuszenia skokowego
    double Xk;
    //omega wymuszenia
    double OMEGA;
    //EPSILON wymuszenia
    double EPSILON;

    //grawitacja
    double g;

    //warunki początkowe
    double x_0;
    double fi_0;
    double v_0;
    //predkosc kątowa początkowa
    double omega_p;


};

#endif // PARAMETRY_H
