#ifndef OKNOPODSTPARAM_H
#define OKNOPODSTPARAM_H

#include <QDialog>
#include <QCloseEvent>

namespace Ui {
class OknoPodstParam;
}

class OknoPodstParam : public QDialog
{
    Q_OBJECT

public:
    explicit OknoPodstParam(QWidget *parent = 0);
    ~OknoPodstParam();
    //masa bloczka
    double M;

    //masa wahadła
    double m;
    //długosc wahadła
    double L;

    //sprężystość sprężyny
    double k;

    //grawitacja
    double g;

    //warunki początkowe
    double x_0;
    double fi_0;
    double v_0;
    //predkosc kątowa początkowa
    double omega_p;

//    //omega dla sprezyny k
//    double omega_0;

    void pobierzParametry();
    Ui::OknoPodstParam *ui;
private slots:
    void on_buttonBox_accepted();

private:


};

#endif // OKNOPODSTPARAM_H
