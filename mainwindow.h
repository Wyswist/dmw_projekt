#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts/QChartView>
#include <QtCharts>
#include "oknopodstparam.h"
#include "modelmatematyczny.h"
#include "parametry.h"
#include "pomoc.h"

QT_CHARTS_USE_NAMESPACE

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_przycisk_start_clicked();
    void on_podsParam_clicked();

    void on_przycisk_wyjdz_clicked();

private:
    Ui::MainWindow *ui;
    QLineSeries *wymuszenieSeria;
    QLineSeries *bloczekSeria;
    QLineSeries *wahadloSeria;
    QChart *chartWymuszenie;
    QChart *chartBloczek;
    QChart *chartWahadlo;
    OknoPodstParam *noweOkno;
    Pomoc *pomocOkno;
    Parametry parametry;
    void zeSpinBoxow(Parametry &p);
    void ustawPodstawoweParam();
};

#endif // MAINWINDOW_H
