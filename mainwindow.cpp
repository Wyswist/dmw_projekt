#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
//#include "ui_oknopodstparam.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    noweOkno = new OknoPodstParam();
    pomocOkno = new Pomoc();
    ustawPodstawoweParam();

    wymuszenieSeria = new QLineSeries();
    chartWymuszenie = new QChart();
    chartWymuszenie->legend()->hide();
    chartWymuszenie->addSeries(wymuszenieSeria);
    chartWymuszenie->createDefaultAxes();
    chartWymuszenie->setAxisX(new QValueAxis(),wymuszenieSeria);
    chartWymuszenie->setTitle("Wymuszenie");
    ui->WykresWymuszenie->setChart(chartWymuszenie);

    bloczekSeria = new QLineSeries();
    chartBloczek = new QChart();
    chartBloczek->legend()->hide();
    chartBloczek->addSeries(bloczekSeria);
    chartBloczek->createDefaultAxes();
    chartBloczek->setAxisX(new QValueAxis(),bloczekSeria);
    chartBloczek->setTitle("Odpowiedź bloczka");
    ui->WykresBloczka->setChart(chartBloczek);

    wahadloSeria = new QLineSeries();
    chartWahadlo = new QChart();
    chartWahadlo->legend()->hide();
    chartWahadlo->addSeries(wahadloSeria);
    chartWahadlo->createDefaultAxes();
    chartWahadlo->setAxisX(new QValueAxis(),wahadloSeria);
    chartWahadlo->setTitle("Odpoweidz wahadła");
    ui->WykresWahada->setChart(chartWahadlo);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_przycisk_start_clicked()
{
    double OMEGA1 = ui->OMEGA1->value();
    double AMPLITUDA1 = ui->AMPLITUDA1->value();
    double EPSILON1 = ui->EPSILON1->value();

    parametry.OMEGA = OMEGA1;
    parametry.Xk = AMPLITUDA1;
    parametry.EPSILON = EPSILON1;
    ustawPodstawoweParam();

//    parametry.omega_0 = noweOkno->omega_0;

    ModelMatematyczny model(parametry);

    wymuszenieSeria->clear();
    bloczekSeria->clear();
    wahadloSeria->clear();
    QPointF maxOdpMasy;
    QPointF maxOdpWahadla;
    int i=0;
    for(double t=0; t<=10; t+=0.1)
    {

        *wymuszenieSeria << QPointF(t,model.Wymuszenie(t));
        *bloczekSeria << QPointF(t,model.OdpowiedzMasy(t));
        *wahadloSeria << QPointF(t,model.OdpowiedzWahadla(t));
        if(t==0)
        {
            maxOdpMasy = bloczekSeria->at(i);
            maxOdpWahadla = wahadloSeria->at(i);
        }
        else
        {
            if(bloczekSeria->at(i).y() > maxOdpMasy.y() )
                maxOdpMasy = bloczekSeria->at(i);

            if(wahadloSeria->at(i).y() > maxOdpWahadla.y() )
                maxOdpWahadla = wahadloSeria->at(i);
        }
        i++;
//        qDebug() << QPointF(t,model.OdpowiedzWahadla(t));
    }
    for(double t=0; t<=10; t+=0.1)
    {

    }
    chartWymuszenie->axisX()->setRange(0, 10);
    chartWymuszenie->axisY()->setRange(-parametry.Xk, parametry.Xk);

    chartBloczek->axisX()->setRange(0, 10);
    chartBloczek->axisY()->setRange(-maxOdpMasy.y()*2.0, maxOdpMasy.y()*2.0);

    chartWahadlo->axisX()->setRange(0, 10);
    chartWahadlo->axisY()->setRange(-M_PI, M_PI);
}


void MainWindow::on_podsParam_clicked()
{
    noweOkno->show();
}

void MainWindow::ustawPodstawoweParam()
{

    parametry.M = noweOkno->M;
    parametry.m = noweOkno->m;
    parametry.L = noweOkno->L;
    parametry.k = noweOkno->k;
    parametry.g = noweOkno->g;
    parametry.x_0 = noweOkno->x_0;
    parametry.fi_0 = noweOkno->fi_0;
    parametry.v_0 = noweOkno->v_0;
    parametry.omega_p = noweOkno->omega_p;
}

void MainWindow::on_przycisk_wyjdz_clicked()
{
    pomocOkno->show();
}
