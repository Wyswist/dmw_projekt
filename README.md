# DMW - Vibrating pendulum
A program that calculates and simulates the movement of a vibrating pendulum. Lagrange equation was used
[Image](https://drive.google.com/file/d/17LZUKlq8gIm5XnhsneF_Yo7TdMGEYMWv/view?usp=sharing)