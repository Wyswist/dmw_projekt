#ifndef MODELMATEMATYCZNY_H
#define MODELMATEMATYCZNY_H

#include "parametry.h"
#include "math.h"
#include "parametrypomocnicze.h"



class ModelMatematyczny
{
public:
    ModelMatematyczny(Parametry p) ;
    double Wymuszenie(double t);
    double OdpowiedzMasy(double t);
    double OdpowiedzWahadla(double t);

private:
    Parametry parametry;
    ParametryPomocnicze parametryP;

};

#endif // MODELMATEMATYCZNY_H

