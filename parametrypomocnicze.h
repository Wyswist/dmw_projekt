#ifndef PARAMETRYPOMOCNICZE_H
#define PARAMETRYPOMOCNICZE_H
#include "parametry.h"
#include "math.h"

class ParametryPomocnicze
{
public:
    ParametryPomocnicze(Parametry param);

    //połowa długości wahadła
    double b;

    //moment bezwładnosci pręta
    double I;

    //omega_1 i epsilon_1 bez wymuszen
    double omega_1;
    double epsilon_1;
    //omega_2 i epsilon_2 bez wymuszen
    double omega_2;
    double epsilon_2;

    //amplituda drgan masy w układzie jednorodnym
    double a_11;
    //amplituda drgan masy w ukladzie niejednorodnych
    double a_12;

    //amplituda drgan wahadła w ukladzie jednorodnych
    double a_21;
    //amplituda drgan wahadła w ukladzie niejednorodnych
    double a_22;

    //współczynniki beta
    double beta_1;
    double beta_2;

    //amplitudy dla układu niejednorodnego
    double B_1;
    double B_2;

    double wspolczynnik_a, wspolczynnik_b, wspolczynnik_c, delta;

    //omega dla sprezyny k
    double omega_0;

private:
    Parametry param;
    double policzB();
    double policzI();
    double policzBeta_1();
    double policzBeta_2();
    void policzDelteIwsp();
    double policzOmega_1();
    double policzEpsilon_1();
    double policzOmega_2();
    double policzEpsilon_2();
    double policz_a_11();
    double policz_a_12();
    double policz_a_21();
    double policz_a_22();
    double policz_B_1();
    double policz_B_2();
    double policzOmega_0();
};



#endif // PARAMETRYPOMOCNICZE_H
