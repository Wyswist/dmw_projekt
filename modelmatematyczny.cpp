#include "modelmatematyczny.h"

//#define parametryP parametrypomocnicze
ModelMatematyczny::ModelMatematyczny(Parametry p):parametry(p), parametryP(ParametryPomocnicze(p))
{

}

double ModelMatematyczny::Wymuszenie(double t)
{
   return parametry.Xk * sin(parametry.OMEGA * t + parametry.EPSILON);
   //   return parametry.Xk_0 * sin(parametry.OMEGA*t+parametry.EPSILON);
}

double ModelMatematyczny::OdpowiedzMasy(double t)
{


    double var = parametryP.a_11 * sin(parametryP.omega_1 * t +parametryP.epsilon_1) + parametryP.a_12 * sin(parametryP.omega_2 + parametryP.epsilon_2) + parametryP.B_1;
    return var;
 //    return sin(7*t);
//    return parametry.Xk * sin(3*parametry.OMEGA*t+parametry.EPSILON);

}

double ModelMatematyczny::OdpowiedzWahadla(double t)
{
    double var = parametryP.beta_1 * parametryP.a_11 * sin(parametryP.omega_1 * t + parametryP.epsilon_1) + parametryP.a_12 * parametryP.beta_2 * sin(parametryP.omega_2 + parametryP.epsilon_2) + parametryP.B_2;
    return asin(sin(var));
    //    return sin(17*t);
//    return 2*parametry.Xk * sin(parametry.OMEGA*t+parametry.EPSILON);
}
//#undef parametryP
